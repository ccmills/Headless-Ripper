#Headless-Ripper#

Based off ofGDVD, 0.8 - Greg's automatic DVD encoder
 
The original script used hal and an outdated version of HandbrakeCLI, I've been working through fixing them


##Prereqs##
In order to run this script `apt-get install` the following:
<ul> 
<li> HandBrakeCLI - for the encoding </li>
<li> lsdvd - to retrieve the title of the DVD </li>
</ul>
and add a rule to /etc/udev/rules.d (you can add a line like the following to 70-persistent-cd.rules):

`"SUBSYSTEM=="block", KERNEL=="sr[0-9]", ACTION=="change", RUN+="/usr/local/bin/pgmrun" , ENV{GENERATED}="1"`

your system specifics may differ, but you can look for events by running udevadm monitor and ejecting/closing your optical drives. 

Then place these files in the following locations;
/usr/local/bin/pgmrun 
/usr/local/bin/dvdinsert

(or wherever you choose to store your scripts)

Chmod to 755 and allow them to be excuted, throw in a DVD and enjoy! :)

##Usage##

Since hal has been deprecated in the latest versions of ubuntu, this script uses a udev rule to watch for changes in /dev/sr0 and trigger pgmrun. Pgmrun is essentially a relay to start the main script, dvdinsert, because udev doesn't like to wait long for things it's called to run. I found I had to be explicit with the PATH in pgmrun to get it to work. I'm still investigating, but I think it's because udev runs things with virtually no context.

There are bugs in the current version that prevent it from running correctly 100% of the time. since the udev event `ACTION=="change"` covers both insertion and ejection of the drive, occasionally the script will try to run multiple times. Additionally, I'm not entirely certain what other system events might create the `ACTION=="change"` flag, which would unintentionally trigger the script. I've begun putting in the stupidest of locks to prevent processes like cp from running concurrently, but I'm now thinking the main loop ought to be split into two separate scripts, one to copy files and one to encode them. In the end, it would be nice to be able to rip CDs into their temporary directory as fast as your machine could copy them, and allow multiple handbrake instances to encode them, but I haven't delved that far into the weeds of how HandBrakeCLI works

On the Handbrake side, the ripped directory is encoded with x264 and two audio tracks : Dolby Digital II and ac3 pass-through. This should give the resulting .mkv flexibility in decoding - DDII is widely supported, and ac3 can be sent over optical and decoded by suround systems


